<?php

namespace App\Http\Controllers;

use App\Events\TypingEvent;
use App\Events\MessageSent;
use App\Message;
use Illuminate\Http\Request;

class ChatsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('chats');
    }

    public function fetchMessages()
    {
        return Message::with('user')->get();
    }

    public function sendMessage(Request $request)
    {
        $message = auth()->user()->messages()->create([
            'message' => $request->message
        ]);

        broadcast(new MessageSent($message->load('user'), $message))->toOthers();
        // event(new MessageSent($message));


        return ['status' => 'success'];
    }

    public function typingNotify(Request $req)
    {
        broadcast(new TypingEvent($req->user(), null))->toOthers();
        // event(new TypingEvent(auth()->user(),null))->toOthers();
        return ['status' => 'success'];
    }
}
